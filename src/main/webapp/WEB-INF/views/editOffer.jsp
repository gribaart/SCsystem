<%--
  Created by IntelliJ IDEA.
  User: artgr
  Date: 07.01.2017
  Time: 14:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("ul.list-group").sortable({
                connectWith: "ul"
            });
            $("#sortable1, #sortable2").disableSelection();
        });
    </script>
    <script language="javascript">
        function submitForms() {
            document.getElementById("offerForm").submit();
        }
    </script>
    <style>
        #sortable1, #sortable2 {
            border: 1px solid #eee;
            width: 142px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable1 li, #sortable2 li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            font-size: 1.2em;
            width: 120px;
        }
    </style>
</head>
<body>
<div class="section" draggable="true">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <li>
                            <a href="addevent">Add Event</a>
                        </li>
                        <li>
                            <a href="addsubject">Add Subject</a>
                        </li>
                    </sec:authorize>
                    <li>
                        <a href="events">Event</a>
                    </li>
                    <li>
                        <a href="users">User list</a>
                    </li>
                    <li>
                        <a href="meeting">Meeting</a>
                    </li>
                    <li class="active">
                        <a href="profile">Profile</a>
                    </li>
                    <li>
                        <a href="logout">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <h4>My offer subject list
                        <button onclick="submitForms()" class="btn btn-default btn-xs">Save</button>
                    </h4>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Unselected subject</th>
                        <th>Offer subject</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <form:form method="post" action="profile1" id="allForm" modelAttribute="allForm">
                                <ul id="sortable1" class="list-group ">
                                    <c:forEach items="${allForm.subjects}" var="subject" varStatus="status">
                                        <li class="list-group-item ">
                                                ${subject.name}
                                            <input name="subjects[${status.index + 1}].name" value="${subject.name}"
                                                   hidden/>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </form:form>
                        </td>
                        <td>
                            <form:form method="post" action="edit_offer" id="offerForm" modelAttribute="offerForm">
                                <ul id="sortable2" class="list-group ">
                                    <c:forEach items="${offerForm.subjects}" var="subject" varStatus="status">
                                        <li class="list-group-item ">
                                                ${subject.name}
                                            <input name="subjects[${status.index + 100}].name" value="${subject.name}"
                                                   hidden/>

                                        </li>
                                    </c:forEach>
                                    <li hidden>
                                        <input name="subjects[0].name" value="NULL"
                                               hidden/>
                                    </li>
                                </ul>
                            </form:form>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
