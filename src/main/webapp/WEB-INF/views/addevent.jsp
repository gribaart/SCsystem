<%--
  Created by IntelliJ IDEA.
  User: artgr
  Date: 28.12.2016
  Time: 11:45
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet"
          type="text/css">
</head>
<body>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <li class="active">
                        <a href="addevent">Add Event</a>
                    </li>
                    <li>
                        <a href="addsubject">Add Subject</a>
                    </li>
                    <li>
                        <a href="events">Event</a>
                    </li>
                    <li>
                        <a href="users">User list</a>
                    </li>
                    <li>
                        <a href="meeting">Meeting</a>
                    </li>
                    <li>
                        <a href="profile">Profile</a>
                    </li>
                    <li>
                        <a href="logout">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form:form method="POST" action="/addevent" modelAttribute="eventForm" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="name" class="control-label">Event</label>
                        </div>
                        <spring:bind path="name">
                            <div class="col-sm-10">
                                <form:input type="text" path="name" id="name" class="form-control"
                                            placeholder="Name of event"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="description" class="control-label">Description</label>
                        </div>
                        <spring:bind path="description">
                            <div class="col-sm-10">
                                <form:input type="text" path="description" class="form-control" id="description"
                                            placeholder="Description"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="adres" class="control-label">Adres</label>
                        </div>
                        <spring:bind path="adres">
                            <div class="col-sm-10">
                                <form:input type="text" path="adres" id="adres" class="form-control" placeholder="Adres"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="date" class="control-label">Data/Time</label>
                        </div>
                        <spring:bind path="date">
                            <div class="col-sm-10">
                                <form:input type="text" path="date" id="date" class="form-control"
                                            placeholder="Data and Time"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button class="btn btn-default" type="submit">Add</button>
                        </div>
                    </div>
                </form:form>

                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Adres</th>
                        <th>Data/Time</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>


                    <c:forEach items="${events}" var="event">
                        <c:url var="deleteUrl" value="/addevent_delete?id=${event.id}"/>
                        <tr>
                            <td>${event.id}</td>
                            <td>${event.name}</td>
                            <td>${event.description}</td>
                            <td>${event.adres}</td>
                            <td>${event.date}</td>
                            <td><a href="${deleteUrl}" class="btn btn-primary">delete</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</body>
</html>
