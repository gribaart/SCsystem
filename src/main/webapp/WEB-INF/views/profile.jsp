<%--
  Created by IntelliJ IDEA.
  User: artgr
  Date: 27.12.2016
  Time: 10:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet"
          type="text/css">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("ul.list-group").sortable({
                connectWith: "ul"
            });
            $("#sortable1, #sortable2, #sortable3").disableSelection();
//            window.location.reload();
        });
    </script>
    <script language="javascript">
        function submitForms() {
            document.getElementById("allForm").submit();
            document.getElementById("desireForm").submit();
            document.getElementById("offerForm").submit();
        }
    </script>
    <style>
        #sortable1, #sortable2, #sortable3 {
            border: 1px solid #eee;
            width: 142px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable1 li, #sortable2 li, #sortable3 li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            font-size: 1.2em;
            width: 120px;
        }
    </style>
</head>
<body>
<div class="section" draggable="true">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <li>
                            <a href="addevent">Add Event</a>
                        </li>
                        <li>
                            <a href="addsubject">Add Subject</a>
                        </li>
                    </sec:authorize>
                    <li>
                        <a href="events">Event</a>
                    </li>
                    <li>
                        <a href="users">User list</a>
                    </li>
                    <li>
                        <a href="meeting">Meeting</a>
                    </li>
                    <li class="active">
                        <a href="profile">Profile</a>
                    </li>
                    <li>
                        <a href="logout">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>My personal information <a href="edit" class="btn btn-default btn-xs"> Edit personal information</a></h3>

                <table class="table table-condensed ">
                    <thead>
                    <tr>
                        <td>Name</td>
                        <td>${user.name}</td>
                    </tr>
                    <tr >
                        <td>Surname</td>
                        <td>${user.surname}</td>
                    </tr >
                    <tr>
                        <td>Mail</td>
                        <td>${user.mail}</td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td>${user.city}</td>
                    </tr>
                    <tr>
                        <td>University</td>
                        <td>${user.university}</td>
                    </tr>
                    <tr>
                        <td>Faculty</td>
                        <td>${user.faculty}</td>
                    </tr>
                    <tr>
                        <td>Specialty</td>
                        <td>${user.specialty}</td>
                    </tr><tr>
                        <td>Facebook</td>
                        <td>${user.facebook}</td>
                    </tr>
                    </thead>
                </table>
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-md-12">
                    <h3>My events</h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Adres</th>
                            <th>Data/Time</th>
                            <th>Invite</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${events}" var="event">
                            <c:url var="unfollowUrl" value="/events_unfollow?id=${event.id}"/>
                            <tr class="warning">
                                <td>${event.name}</td>
                                <td>${event.description}</td>
                                <td>${event.adres}</td>
                                <td>${event.date}</td>
                                <td>
                                    <a href="${unfollowUrl}" class="btn btn-primary">unfollow</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <h4>My subject list</h4>
                </div>
                <table class="table">
                    <thead>

                    <tr>
                        <th>Desire
                            <a href="edit_desire" class="btn btn-default btn-xs">Update</a>
                        </th>
                        <th>Offer
                            <a href="edit_offer" class="btn btn-default btn-xs">Update</a>
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <ul class="list-group ">
                                <c:forEach items="${desireForm.subjects}" var="subject" varStatus="status">
                                    <li class="list-group-item ">
                                            ${subject.name}
                                    </li>
                                </c:forEach>
                            </ul>
                        </td>
                        <td>
                            <ul class="list-group ">
                                <c:forEach items="${offerForm.subjects}" var="subject" varStatus="status">
                                    <li class="list-group-item ">
                                            ${subject.name}
                                    </li>
                                </c:forEach>
                            </ul>
                        </td>

                    </tr>
                    </tbody>
                </table>


                <%--</form:form>--%>


            </div>
        </div>
    </div>
</div>
</body>
</html>
