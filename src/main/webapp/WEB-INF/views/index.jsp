<%--
  Created by IntelliJ IDEA.
  User: artgr
  Date: 30.12.2016
  Time: 23:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript"
                src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
              type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet"
              type="text/css">
    </head>
</head>

<body>
<div class="section" draggable="true">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <sec:authorize access="isAuthenticated()">
                    <ul class="nav nav-pills">
                        <sec:authorize access="hasAuthority('ADMIN')">
                            <li>
                                <a href="addevent">Add Event</a>
                            </li>
                            <li>
                                <a href="addsubject">Add Subject</a>
                            </li>
                        </sec:authorize>
                        <li>
                            <a href="events">Event</a>
                        </li>
                        <li>
                            <a href="users">User list</a>
                        </li>
                        <li>
                            <a href="meeting">Meeting</a>
                        </li>
                        <li>
                            <a href="profile">Profile</a>
                        </li>
                        <li>
                            <a href="logout">Log out</a>
                        </li>
                    </ul>
                </sec:authorize>
            </div>
        </div>
    </div>
</div>
<div class="section" draggable="true">
    <div class="container">
            <div class="col-md col-md-offset-4">
                <img src="/resources/img/logo.png" class="img-responsive" alt="Cinque Terre">
            </div>

    </div>
</div>
<div class="section" draggable="true">
    <div class="container">
        <div class="jumbotron" style="margin-top: 20px;">
            <h1>StudentCommunicationSystem.com</h1>
            <p class="lead">
                Student’s Communication System is a web application which main purpose is to allow students to
                exchange
                knowledge and information. The main advantage of this system is that it gives you the opportunity to
                seek
                someone who can help you with a specific learning subject, it will also be free of charge and will
                become a
                community of knowledge.
            </p>
            <sec:authorize access="!isAuthenticated()">
                <p><a class="btn btn-lg btn-success" href="<c:url value="/login" />" role="button">Log in</a></p>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <p>Your login: <sec:authentication property="principal.username"/></p>
                <p><a class="btn btn-lg btn-danger" href="<c:url value="/edit" />" role="button">Complete your
                    profile
                    information</a></p>

            </sec:authorize>
        </div>
        <div class="footer">
            <p>© SIT FEL CVUT 2016/2017</p>
        </div>
    </div>
</div>
</body>
</html>