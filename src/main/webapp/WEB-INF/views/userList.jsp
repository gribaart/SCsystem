<%--
  Created by IntelliJ IDEA.
  User: artgr
  Date: 27.12.2016
  Time: 18:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet"
          type="text/css">
</head>
<body>
<div class="section" draggable="true">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <li>
                            <a href="addevent">Add Event</a>
                        </li>
                        <li>
                            <a href="addsubject">Add Subject</a>
                        </li>
                    </sec:authorize>
                    <li>
                        <a href="events" >Event</a>
                    </li>
                    <li class="active">
                        <a href="users">User list</a>
                    </li>
                    <li>
                        <a href="meeting">Meeting</a>
                    </li>
                    <li>
                        <a href="profile">Profile</a>
                    </li>
                    <li>
                        <a href="logout">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name/Surname</th>
                        <th>University</th>
                        <th>Invite</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${bestUserSet}" var="user">
                        <c:url var="requestUrl" value="/send?id=${user.id}" />
                        <tr class="success">
                            <td><a href="/page?id=${user.id}" >${user.name} ${user.surname}</a></td>
                            <td>${user.university}</td>
                            <td><a href="${requestUrl}" class="btn btn-primary btn-xs">send a request</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <tbody>
                    <c:forEach items="${bigUserSet}" var="user">
                        <c:url var="requestUrl" value="/send?id=${user.id}" />
                        <tr class="active">
                            <td><a href="/page?id=${user.id}" >${user.name} ${user.surname}</a></td>
                            <td>${user.university}</td>
                            <td><a href="${requestUrl}" class="btn btn-primary btn-xs">send a request</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
