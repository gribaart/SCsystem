<%--
  Created by IntelliJ IDEA.
  User: artgr
  Date: 27.12.2016
  Time: 18:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet"
          type="text/css">
</head>
<body>
<div class="section" draggable="true">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <li>
                            <a href="addevent">Add Event</a>
                        </li>
                        <li>
                            <a href="addsubject">Add Subject</a>
                        </li>
                    </sec:authorize>
                    <li>
                        <a href="events">Event</a>
                    </li>
                    <li>
                        <a href="users">User list</a>
                    </li>
                    <li class="active">
                        <a href="meeting">Meeting</a>
                    </li>
                    <li>
                        <a href="profile">Profile</a>
                    </li>
                    <li>
                        <a href="logout">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th>New</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${newMeetings}" var="meeting">
                        <c:url var="confirmUrl" value="/confirm?id=${meeting.id}"/>
                        <c:url var="deleteUrl" value="/dismiss?id=${meeting.id}"/>
                        <tr class="info">
                            <td>
                                <a href="/page?id=${meeting.applicant.id}">${meeting.applicant.name} ${meeting.applicant.surname}</a>
                            </td>
                            <td><a href="${confirmUrl}" class="btn btn-default btn-xs">Confirm</a>
                                <a href="${deleteUrl}" class="btn btn-default btn-xs">Dismiss</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Confirmed</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${conffirmedMeetings}" var="meeting">
                        <tr class="success">
                            <c:if test="${meeting.answerer.mail == person.mail}">
                                <td>
                                    <a href="/page?id=${meeting.applicant.id}">${meeting.applicant.name} ${meeting.applicant.surname}</a>
                                </td>
                                <td><a href="${meeting.applicant.facebook}">${meeting.applicant.facebook}</a></td>
                            </c:if>
                            <c:if test="${meeting.applicant.mail == person.mail}">
                                <td>
                                    <a href="/page?id=${meeting.answerer.id}">${meeting.answerer.name} ${meeting.answerer.surname}</a>
                                </td>
                                <td><a href="${meeting.answerer.facebook}">${meeting.answerer.facebook}</a></td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Sent</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${sentMeetings}" var="meeting">
                        <tr class="warning">
                            <td>
                                <a href="/page?id=${meeting.answerer.id}">${meeting.answerer.name} ${meeting.answerer.surname}</a>
                            </td>
                            <td></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
