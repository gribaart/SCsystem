<%--
  Created by IntelliJ IDEA.
  User: artgr
  Date: 05.01.2017
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet"
          type="text/css">
</head>
<body>
<div class="section" draggable="true">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <li>
                            <a href="addevent">Add Event</a>
                        </li>
                        <li>
                            <a href="addsubject">Add Subject</a>
                        </li>
                    </sec:authorize>
                    <li>
                        <a href="events">Event</a>
                    </li>
                    <li>
                        <a href="users">User list</a>
                    </li>
                    <li>
                        <a href="meeting">Meeting</a>
                    </li>
                    <li>
                        <a href="profile">Profile</a>
                    </li>
                    <li>
                        <a href="logout">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form:form method="POST" modelAttribute="editForm" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="name" class="control-label">Name</label>
                        </div>
                        <spring:bind path="name">
                            <div class="col-sm-10">
                                <form:input type="text" path="name" id="name" class="form-control"
                                            placeholder="Name"
                                            value="${person.name}"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                        <div class="col-sm-2">
                            <label for="surname" class="control-label">Surname</label>
                        </div>
                        <spring:bind path="surname">
                            <div class="col-sm-10">
                                <form:input type="text" path="surname" id="surname" class="form-control"
                                            placeholder="Surname"
                                            value="${person.surname}"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                        <div class="col-sm-2">
                            <label for="mail" class="control-label">Mail</label>
                        </div>
                        <spring:bind path="mail">
                            <div class="col-sm-10">
                                <form:input type="text" path="mail" id="mail" class="form-control"
                                            placeholder="Your e-mail/login"
                                            value="${person.mail}"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                        <div class="col-sm-2">
                            <label for="password" class="control-label">Password</label>
                        </div>
                        <spring:bind path="password">
                            <div class="col-sm-10">
                                <form:input type="text" path="password" id="password" class="form-control"
                                            placeholder="Your password"
                                            value="${person.password}"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                        <div class="col-sm-2">
                            <label for="city" class="control-label">City</label>
                        </div>
                        <spring:bind path="city">
                            <div class="col-sm-10">
                                <form:input type="text" path="city" id="city" class="form-control"
                                            placeholder="Name of your city"
                                            value="${person.city}"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                        <div class="col-sm-2">
                            <label for="university" class="control-label">University</label>
                        </div>
                        <spring:bind path="university">
                            <div class="col-sm-10">
                                <form:input type="text" path="university" id="name" class="form-control"
                                            placeholder="Name of your university"
                                            value="${person.university}"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                        <div class="col-sm-2">
                            <label for="faculty" class="control-label">Faculty</label>
                        </div>
                        <spring:bind path="faculty">
                            <div class="col-sm-10">
                                <form:input type="text" path="faculty" id="faculty" class="form-control"
                                            placeholder="Name of your faculty"
                                            value="${person.faculty}"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                        <div class="col-sm-2">
                            <label for="specialty" class="control-label">Specialty</label>
                        </div>
                        <spring:bind path="specialty">
                            <div class="col-sm-10">
                                <form:input type="text" path="specialty" id="specialty" class="form-control"
                                            placeholder="Name of your specialty"
                                            value="${person.specialty}"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                        <div class="col-sm-2">
                            <label for="facebook" class="control-label">Facebook</label>
                        </div>
                        <spring:bind path="specialty">
                            <div class="col-sm-10">
                                <form:input type="text" path="facebook" id="facebook" class="form-control"
                                            placeholder="Your facebook link"
                                            value="${person.facebook}"
                                            autofocus="true"></form:input>
                            </div>
                        </spring:bind>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button class="btn btn-default" type="submit">Update</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
</head>
<body>

</body>
</html>
