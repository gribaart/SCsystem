/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.service;

import com.mycompany.spring.model.Person;

import java.util.List;

/**
 *
 * @author artgr
 */
//@Transactional
public interface PersonService {

    Person addPerson(Person person);

    Person findByMail(String mail);

    void delete(Integer id);

//    Person getByName(String name);

    Person editPerson(Person person);

    List<Person> getAll();

    Person findOne(Integer id);

}
