/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.service;

import com.mycompany.spring.model.Comment;
import com.mycompany.spring.model.Person;
import java.util.List;

/**
 *
 * @author artgr
 */
public interface CommentService {

    Comment addComment(Comment comment);

    void delete(Integer id);

    List<Comment> getBySender(Person person);

    List<Comment> getByAdressee(Person person);

    Comment editComment(Comment comment);

    List<Comment> getAll();
}
