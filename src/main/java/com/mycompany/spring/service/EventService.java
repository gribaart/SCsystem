/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.service;

import com.mycompany.spring.model.Event;
import com.mycompany.spring.model.Person;

import java.util.List;

/**
 * @author artgr
 */
public interface EventService {

    Event addEvent(Event event);

    void delete(Integer id);

    Event getByName(String name);

    Event getById(Integer id);

//    List<Event> getByPersonId(Integer id);

    Event editEvent(Event event);

    List<Event> getAll();
}
