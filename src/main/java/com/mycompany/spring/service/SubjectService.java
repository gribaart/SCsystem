/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.service;

import com.mycompany.spring.model.Subject;
import java.util.List;

/**
 *
 * @author artgr
 */
public interface SubjectService {

    Subject getByName(String name);

    Subject getById(Integer id);

    Subject addSubject(Subject subject);

    void delete(Integer id);

    Subject editSubject(Subject subject);

    List<Subject> getAll();
}
