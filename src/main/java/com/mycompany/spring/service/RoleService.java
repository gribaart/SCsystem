package com.mycompany.spring.service;

import com.mycompany.spring.model.Role;

import java.util.List;

/**
 * Created by artgr on 11/29/2016.
 */
public interface RoleService {
    Role addRole(Role role);

    Role getByRole(String name);

    void delete(Integer id);

    Role editRole(Role role);

    List<Role> getAll();
}
