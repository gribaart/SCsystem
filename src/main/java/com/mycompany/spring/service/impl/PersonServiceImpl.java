/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.service.impl;

import com.mycompany.spring.model.Event;
import com.mycompany.spring.model.Subject;
import com.mycompany.spring.repository.EventRepository;
import com.mycompany.spring.repository.PersonRepository;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.repository.SubjectRepository;
import org.springframework.stereotype.Service;
import com.mycompany.spring.service.PersonService;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author artgr
 */
@Service
public class PersonServiceImpl implements PersonService {
    private static Logger log = Logger.getLogger(PersonServiceImpl.class.getName());

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    public Person addPerson(Person person) {
        log.info("Try save and flash user");
        return personRepository.save(person);
    }

    public Person findByMail(String mail) {
        return personRepository.findByMail(mail);
    }


    public void delete(Integer id) {
        Person person = personRepository.findById(id);
        Set<Event> event = person.getEvents();
        Set<Subject> desire = person.getDesire();
        Set<Subject> offer = person.getOffer();
        for(Event e : event){
            e.getParticipants().remove(person);
            eventRepository.saveAndFlush(e);
        }
        for(Subject sub : desire){
            sub.getStudentsDesire().remove(person);
            subjectRepository.saveAndFlush(sub);
        }
        for(Subject sub : offer){
            sub.getStudentsOffer().remove(person);
            subjectRepository.saveAndFlush(sub);
        }
        personRepository.delete(id);
    }


    public Person getByName(String name) {
        return personRepository.findByName(name);
    }


    public Person editPerson(Person person) {
        return personRepository.saveAndFlush(person);
    }


    public List<Person> getAll() {
        return personRepository.findAll();
    }

    @Override
    public Person findOne(Integer id) {
        return personRepository.findOne(id);
    }
}
