package com.mycompany.spring.service.impl;

import com.mycompany.spring.model.Meeting;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.repository.MeetingRepository;
import com.mycompany.spring.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by artgr on 11/30/2016.
 */
@Service
public class MeetingServiceImpl implements MeetingService {

    @Autowired
    private MeetingRepository meetingRepository;


    @Override
    public Meeting addMeeting(Meeting meeting) {
        return meetingRepository.save(meeting);
    }

    @Override
    public Meeting findOne(Integer id) {
        return meetingRepository.getOne(id);
    }

    @Override
    public void delete(Integer id) {
        meetingRepository.delete(id);
    }

    @Override
    public Meeting getByApplicant(Person person) {
        return null;
    }

    @Override
    public Meeting getByAnswerer(Person person) {
        return null;
    }

    @Override
    public Meeting editMeeting(Meeting meeting) {
        return meetingRepository.saveAndFlush(meeting);
    }

    @Override
    public List<Meeting> getAll() {
        return meetingRepository.findAll();
    }
}
