package com.mycompany.spring.service.impl;

import com.mycompany.spring.model.Role;
import com.mycompany.spring.repository.RoleRepository;
import com.mycompany.spring.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by artgr on 11/30/2016.
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role addRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role getByRole(String name) {return roleRepository.findByRole(name);}

    @Override
    public void delete(Integer id) {
        roleRepository.delete(id);
    }

    @Override
    public Role editRole(Role role) {
        return roleRepository.saveAndFlush(role);
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.findAll();
    }
}
