package com.mycompany.spring.service.impl;

import com.mycompany.spring.repository.EventRepository;
import com.mycompany.spring.model.Event;
import com.mycompany.spring.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by artgr on 11/29/2016.
 */
@Service
public class EventServiceImpl implements EventService {
    @Autowired
    private EventRepository eventRepository;

    @Override
    public Event addEvent(Event event) {
        Event savedEvent = eventRepository.save(event);
        return savedEvent;
    }

    @Override
    public void delete(Integer id) {
        eventRepository.delete(id);
    }

    //    @Override
//    public List<Event> getByPersonId(Integer id) {
//        return eventRepository.findByPersonId(id);
//    }
    @Override
    public Event getById(Integer id) {
        return eventRepository.findById(id);
    }


    @Override
    public Event getByName(String name) {return eventRepository.findByName(name);}


    @Override
    public Event editEvent(Event event) {
        return eventRepository.saveAndFlush(event);
    }

    @Override
    public List<Event> getAll() {
        return eventRepository.findAll();
    }
}
