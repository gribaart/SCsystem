package com.mycompany.spring.service.impl;

import com.mycompany.spring.model.Comment;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.repository.CommentRepository;
import com.mycompany.spring.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by artgr on 11/30/2016.
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Comment addComment(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public void delete(Integer id) {
        commentRepository.delete(id);
    }

    @Override
    public List<Comment> getBySender(Person person) {
        return null;
    }

    @Override
    public List<Comment> getByAdressee(Person person) {
        return null;
    }

    @Override
    public Comment editComment(Comment comment) {
        return commentRepository.saveAndFlush(comment);
    }

    @Override
    public List<Comment> getAll() {
        return commentRepository.findAll();
    }
}
