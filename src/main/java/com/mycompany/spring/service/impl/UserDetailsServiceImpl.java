package com.mycompany.spring.service.impl;

import com.mycompany.spring.model.Enum.UserRoleEnum;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by artgr on 30.12.2016.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private PersonServiceImpl personService;

    @Override
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
        Person user = personService.findByMail(mail);
        Set<GrantedAuthority> roles = new HashSet();
        try {
            for (Role role : user.getRoles()) {
                switch (role.getRole()) {
                    case "ADMIN":
                        roles.add(new SimpleGrantedAuthority("ADMIN"));
                        System.out.println(role.getRole() + " Hi!");
                        break;
                    case "USER":
                        roles.add(new SimpleGrantedAuthority("USER"));
                        System.out.println(role.getRole() + " Hi!");
                        break;
                }
            }
        }catch (Exception e){}
        UserDetails userDetails =
                new org.springframework.security.core.userdetails.User(user.getMail(),
                        user.getPassword(),
                        roles);
                return userDetails;
    }
}
