package com.mycompany.spring.service.impl;

import com.mycompany.spring.repository.SubjectRepository;
import com.mycompany.spring.model.Subject;
import com.mycompany.spring.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by artgr on 11/29/2016.
 */
@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;


    @Override
    public Subject getByName(String name) {
        return subjectRepository.findByName(name);
    }

    @Override
    public Subject getById(Integer id) {
        return subjectRepository.findById(id);
    }

    @Override
    public Subject addSubject(Subject subject) {
        Subject saveSubject = subjectRepository.saveAndFlush(subject);
        return saveSubject;
    }

    @Override
    public void delete(Integer id) {
        subjectRepository.delete(id);
    }

    @Override
    public Subject editSubject(Subject subject) {
        return subjectRepository.saveAndFlush(subject);
    }

    @Override
    public List<Subject> getAll() {
        return subjectRepository.findAll();
    }
}
