/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.service;

import com.mycompany.spring.model.Meeting;
import com.mycompany.spring.model.Person;
import java.util.List;

/**
 *
 * @author artgr
 */
public interface MeetingService {
    Meeting addMeeting(Meeting meeting);

    Meeting findOne(Integer id);

    void delete(Integer id);

    Meeting getByApplicant(Person person);

    Meeting getByAnswerer(Person person);

    Meeting editMeeting(Meeting meeting);

    List<Meeting> getAll();
}
