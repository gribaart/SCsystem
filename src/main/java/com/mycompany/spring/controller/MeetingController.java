package com.mycompany.spring.controller;

import com.mycompany.spring.model.Meeting;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.service.MeetingService;
import com.mycompany.spring.service.impl.MeetingServiceImpl;
import com.mycompany.spring.service.impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by artgr on 08.01.2017.
 */
@org.springframework.stereotype.Controller
public class MeetingController {
    @Autowired
    private PersonServiceImpl personService;

    @Autowired
    private MeetingServiceImpl meetingService;

    @RequestMapping(value = "/confirm")
    public String meetingConfirm(@RequestParam("id") Integer id, Model model){
        Meeting meeting = meetingService.findOne(id);
        meeting.setApprove(true);
        meetingService.editMeeting(meeting);
        return "redirect:meeting";
    }

    @RequestMapping(value = "/meeting", method = RequestMethod.GET)
    public String meetingGet(Model model) {
        Person person = personService.findByMail(SecurityContextHolder.getContext().getAuthentication().getName());
        List<Meeting> meetings = meetingService.getAll();
        Set<Meeting> newMeetings = new HashSet<>();
        Set<Meeting> conffirmedMeetings = new HashSet<>();
        Set<Meeting> sentMeetings = new HashSet<>();
        for(Meeting meeting : meetings){
            if(meeting.approve == true){
                conffirmedMeetings.add(meeting);
            }else if(meeting.getApplicant().equals(person)){
                    sentMeetings.add(meeting);
            }else if(meeting.getAnswerer().equals(person)){
                    newMeetings.add(meeting);
            }
        }
        model.addAttribute("conffirmedMeetings",conffirmedMeetings);
        model.addAttribute("sentMeetings",sentMeetings);
        model.addAttribute("newMeetings", newMeetings);
        model.addAttribute("person", person);
        return "meetingList";
    }

    @RequestMapping(value = "/send")
    public String meetingPost(@RequestParam("id") Integer id, Model model){
        Person answerer = personService.findOne(id);
        Person applicant = personService.findByMail(SecurityContextHolder.getContext().getAuthentication().getName());
        Meeting meeting = new Meeting();
        meeting.setAnswerer(answerer);
        meeting.setApplicant(applicant);
        meetingService.addMeeting(meeting);
        return "redirect:users";
    }

    @RequestMapping(value = "/dismiss")
    public String meetingDismiss(@RequestParam("id") Integer id, Model model){

        meetingService.delete(id);
        return "redirect:meeting";
    }
}
