package com.mycompany.spring.controller;


import com.mycompany.spring.model.Event;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.service.impl.EventServiceImpl;
import com.mycompany.spring.service.impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

/**
 * Created by artgr on 28.12.2016.
 */
@org.springframework.stereotype.Controller
public class EventController {
    @Autowired
    private EventServiceImpl eventService;

    @Autowired
    private PersonServiceImpl personService;

    //some user get events list page
    // show all unfollow event
    @RequestMapping(value = "/events", method = RequestMethod.GET)
    public String eventsList(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByMail(auth.getName());
        List<Event> eventsList = eventService.getAll();
        Set<Event> updateList = new HashSet<Event>();
        for (Event ev: eventsList) {
            if(!ev.getParticipants().contains(person)){
                updateList.add(ev);
            }
        }
        model.addAttribute("events", updateList);
        return "event";
    }

    //user invated to some event
    @RequestMapping(value = "/events_follow")
    public String followEvent(@RequestParam("id") Integer id, Model model) {
//        if (eventService.getById(id) != null) {
//            eventService.delete(id);
//        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Event event = eventService.getById(id);
        Person person = personService.findByMail(auth.getName());
        event.getParticipants().add(person);
        Set<Event> events = person.getEvents();
        if(events != null){
            events.add(event);
        }else {
            events = new HashSet<Event>();
            events.add(event);
            person.setEvents(events);
        }
        eventService.editEvent(event);
        personService.editPerson(person);
//        model.addAttribute("id", id);
        List<Event> eventsList = eventService.getAll();
        Set<Event> updateList = new HashSet<Event>();
        for (Event ev: eventsList) {
            if(!ev.getParticipants().contains(person)){
                updateList.add(ev);
            }
        }
        model.addAttribute("events", updateList);
        return "event";
    }

    //user invated to some event
    @RequestMapping(value = "/events_unfollow")
    public String unfollowEvent(@RequestParam("id") Integer id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Event event = eventService.getById(id);
        Person person = personService.findByMail(auth.getName());
        event.getParticipants().remove(person);
        Set<Event> events = person.getEvents();
        if(events != null){
            events.remove(event);
        }
        eventService.editEvent(event);
        personService.editPerson(person);
        List<Event> eventsList = eventService.getAll();
        Set<Event> updateList = new HashSet<Event>();
        for (Event ev: eventsList) {
            if(ev.getParticipants().contains(person)){
                updateList.add(ev);
            }
        }
        model.addAttribute("events", updateList);
        return "profile";
    }

    //admin get addevent page
    @RequestMapping(value = "/addevent", method = RequestMethod.GET)
    public String events(Model model) {
        model.addAttribute("eventForm", new Event());
        model.addAttribute("events", eventService.getAll());
        return "addevent";
    }

    //admin add event
    @RequestMapping(value = "/addevent", method = RequestMethod.POST)
    public String addEvent(@ModelAttribute("eventForm") Event eventForm, Model model) {
        if (eventService.getByName(eventForm.getName()) == null && eventForm.getName() != "" && eventForm.getDate() != "" && eventForm.getDescription() != "" && eventForm.getAdress() != "") {
            eventService.addEvent(new Event(eventForm.getName(), eventForm.getDate(), eventForm.getDescription(), eventForm.getAdress()));
        }
        model.addAttribute("eventForm", new Event());
        model.addAttribute("events", eventService.getAll());
        return "addevent";
    }

    //admin delete event
    @RequestMapping(value = "/addevent_delete")
    public String deleteEvent(@RequestParam("id") Integer id, Model model) {
        Event event = eventService.getById(id);
        if ( event != null) {
            for (Person p : event.getParticipants()) {
                p.getEvents().remove(event);
                personService.editPerson(p);
            }
            eventService.delete(id);
        }

        model.addAttribute("id", id);
        model.addAttribute("eventForm", new Event());
        model.addAttribute("events", eventService.getAll());
        return "addevent";

    }



}

