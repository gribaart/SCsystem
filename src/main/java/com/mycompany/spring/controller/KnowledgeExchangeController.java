package com.mycompany.spring.controller;

import com.mycompany.spring.model.Meeting;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.model.Subject;
import com.mycompany.spring.service.impl.MeetingServiceImpl;
import com.mycompany.spring.service.impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by artgr on 08.01.2017.
 */
@org.springframework.stereotype.Controller
public class KnowledgeExchangeController {
    @Autowired
    private PersonServiceImpl personService;
    @Autowired
    private MeetingServiceImpl meetingService;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String users(Model model) {
        Person person = personService.findByMail(SecurityContextHolder.getContext().getAuthentication().getName());
        List<Person> allPersons = personService.getAll();
        allPersons.remove(person);

        Set<Person> bigSetPerson = new HashSet<>();
        Set<Person> bestSetPerson = new HashSet<>();

        Set<Subject> myOffer = person.getOffer();
        Set<Subject> myDesire = person.getDesire();
        List<Meeting> meetings = meetingService.getAll();
        for (Person user : allPersons) {
            boolean firstStep = true;
            for (Meeting meeting : meetings) {
                if ((meeting.getAnswerer().equals(user) && meeting.getApplicant().equals(person))
                        || (meeting.getAnswerer().equals(person) && meeting.getApplicant().equals(user))
                        && meeting.isApprove()) {
                    firstStep = false;
                }
            }
            if (firstStep) {
                boolean containsOne = false;
                boolean containsTwo = false;
                boolean equalsSet = false;

                for (Subject offer : user.getDesire()) {
                    if (person.getDesire().contains(offer)) {
                        containsOne = true;
                        break;
                    }
                }
                for (Subject desire : user.getOffer()) {
                    if (person.getOffer().contains(desire)) {
                        containsTwo = true;
                        break;
                    }
                }
                equalsSet = user.getDesire().equals(person.getOffer()) && user.getOffer().equals(person.getDesire());
                if (equalsSet) {
                    bestSetPerson.add(user);
                } else if (containsOne && containsTwo) {
                    bigSetPerson.add(user);
                }
            }
        }


        model.addAttribute("bigUserSet", bigSetPerson);
        model.addAttribute("bestUserSet", bestSetPerson);
        return "userList";
    }
}
