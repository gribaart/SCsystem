package com.mycompany.spring.controller;

import com.mycompany.spring.model.DTO.AllDTO;
import com.mycompany.spring.model.DTO.DesireDTO;
import com.mycompany.spring.model.DTO.OfferDTO;
import com.mycompany.spring.model.DTO.SubjectDTO;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.model.Subject;
import com.mycompany.spring.service.impl.PersonServiceImpl;
import com.mycompany.spring.service.impl.SubjectServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artgr on 27.12.2016.
 */
@org.springframework.stereotype.Controller
public class Controller {

    @Autowired
    private PersonServiceImpl personService;


    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "index";
    }

    @RequestMapping(value = {"/page"}, method = RequestMethod.GET)
    public String page(@RequestParam("id") Integer id, Model model) {
        Person person = personService.findOne(id);
        List<SubjectDTO> desire = new ArrayList<SubjectDTO>();
        List<SubjectDTO> offer = new ArrayList<SubjectDTO>();
        for (Subject subject : person.getDesire()) {
                desire.add(new SubjectDTO(subject.getName()));
        }
        for (Subject subject : person.getOffer()) {
                offer.add(new SubjectDTO(subject.getName()));
        }

        DesireDTO desireS = new DesireDTO();
        OfferDTO offerS = new OfferDTO();

        offerS.setSubjects(offer);
        desireS.setSubjects(desire);

        model.addAttribute("desireForm", desireS);
        model.addAttribute("offerForm", offerS);
        model.addAttribute("user", person);

        return "userpage";
    }


}


