package com.mycompany.spring.config;

import java.util.Arrays;

import com.mycompany.spring.model.Person;
import com.mycompany.spring.service.impl.PersonServiceImpl;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.ApplicationContext;
/**
 * Created by artgr on 11/27/2016.
 */


@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(new Class<?>[] {Application.class, SpringWebAppInitializer.class}, args);

        System.out.println("Let's inspect the beans provided by Spring Boot:");



    }

}