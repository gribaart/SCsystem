/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class represent comments in user's profile
 * 
 * @author bahlaliz
 */
@Entity
@Table
public class Comment extends BaseEntity implements Serializable{
    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "senderRelation_ID")
    private Person senderRelation;

    @ManyToOne(optional = false)
    @JoinColumn(name = "adresseeRelation_ID")
    private Person adresseeRelation;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;

    @Column
    private String text;

    public Person getSenderRelation() {
        return senderRelation;
    }

    public Person getAdresseeRelation() {
        return adresseeRelation;
    }

    public Date getCreated() {
        return created;
    }

    public String getText() {
        return text;
    }

    public void setSenderRelation(Person senderRelation) {
        this.senderRelation = senderRelation;
    }

    public void setAdresseeRelation(Person adresseeRelation) {
        this.adresseeRelation = adresseeRelation;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setText(String text) {
        this.text = text;
    }

}
