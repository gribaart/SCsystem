package com.mycompany.spring.model.Enum;

/**
 * Created by artgr on 30.12.2016.
 */
public enum UserRoleEnum {

    ADMIN,
    USER,
    UNKNOWN;

    UserRoleEnum() {
    }

}