package com.mycompany.spring.model.DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artgr on 07.01.2017.
 */
public class AllDTO {
    private List<SubjectDTO> subjects = new ArrayList<>();

    public AllDTO() {
    }

    public List<SubjectDTO> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectDTO> subjects) {
        this.subjects = subjects;
    }
}
