package com.mycompany.spring.model.DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artgr on 06.01.2017.
 */
public class FormSubjectDTO {
    private List<SubjectDTO> all = new ArrayList<>();
    private List<SubjectDTO> desire = new ArrayList<>();
    private List<SubjectDTO> offer = new ArrayList<>();

    public FormSubjectDTO() {
    }

    public List<SubjectDTO> getAll() {
        return all;
    }

    public void setAll(List<SubjectDTO> all) {
        this.all = all;
    }

    public List<SubjectDTO> getDesire() {
        return desire;
    }

    public void setDesire(List<SubjectDTO> desire) {
        this.desire = desire;
    }

    public List<SubjectDTO> getOffer() {
        return offer;
    }

    public void setOffer(List<SubjectDTO> offer) {
        this.offer = offer;
    }
}
