/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Class represent users
 *
 * @author bahlaliz
 */
@Entity
@Table
public class Person extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    @Column(unique = true)
    private String mail;

    @Column
    private String password;

    @Column
    private String name;

    @Column
    private String surname;

    @Column
    private String city;

    @Column
    private String university;

    @Column
    private String faculty;

    @Column
    private String specialty;

    @Column
    private String facebook;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "participants")
    private Set<Event> events;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "studentsDesire")
    private Set<Subject> desire;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "studentsOffer")
    private Set<Subject> offer;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;


    public Person(String mail, String password) {

        this.password = password;
        this.mail = mail;
    }

    public Person(Integer id, String mail, String password, String name, String surname, String city, String university, String faculty, String specialty, String facebook) {
        super.setId(id);
        this.mail = mail;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.university = university;
        this.faculty = faculty;
        this.specialty = specialty;
        this.facebook = facebook;
    }

    public Person() {
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public Set<Subject> getOffer() {
        return offer;
    }

    public void setOffer(Set<Subject> offer) {
        this.offer = offer;
    }

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCity() {
        return city;
    }

    public String getUniversity() {
        return university;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public void setDesire(Set<Subject> desire) {
        this.desire = desire;
    }

    public Set<Subject> getDesire() {
        return desire;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }


}
