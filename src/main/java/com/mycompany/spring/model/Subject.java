/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Class represent university subjects
 * 
 * @author bahlaliz
 */
@Entity
@Table
public class Subject extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;




    @Column(unique = true)
    private String name;

    @ManyToMany
    @JoinTable(name = "desire_subject", joinColumns = @JoinColumn(name = "Subject_ID"), inverseJoinColumns = @JoinColumn(name = "Person_ID"))
    private Set<Person> studentsDesire = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "offer_subject", joinColumns = @JoinColumn(name = "Subject_ID"), inverseJoinColumns = @JoinColumn(name = "Person_ID"))
    private Set<Person> studentsOffer =  new HashSet<>();


    public Subject(String name) {
        this.name = name;
    }

    public Subject() {
    }


    public Set<Person> getStudentsDesire() {
        return studentsDesire;
    }

    public Set<Person> getStudentsOffer() {
        return studentsOffer;
    }

    public void setStudentsDesire(Set<Person> studentsDesire) {
        this.studentsDesire = studentsDesire;
    }

    public void setStudentsOffer(Set<Person> studentsOffer) {
        this.studentsOffer = studentsOffer;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.getName() == null) ? 0 : this.getName().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        Subject r = (Subject) obj;
        return this.getName() == r.getName();
    }
    
}
