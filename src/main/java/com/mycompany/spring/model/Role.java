package com.mycompany.spring.model;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by bahlaliz
 */
@Entity
@Table
public class Role extends BaseEntity implements GrantedAuthority  {

    @Column(unique = true)
    private String role;

    @ManyToMany(mappedBy = "roles")
    Set<Person> users;

    public Set<Person> getUsers() {
        return users;
    }

    public void setUsers(Set<Person> users) {
        this.users = users;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    protected Role(){}
    public Role(String name)
    {
        role = name;
    }

    @Override
    public String getAuthority() {
        return getRole();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.getRole() == null) ? 0 : this.getRole().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        Role r = (Role) obj;
        return this.getRole() == r.getRole();
    }
}
