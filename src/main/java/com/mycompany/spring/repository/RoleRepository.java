package com.mycompany.spring.repository;

import com.mycompany.spring.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by artgr on 11/29/2016.
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByRole(String name);
}
