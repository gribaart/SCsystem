/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.repository;

import com.mycompany.spring.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author artgr
 */
public interface SubjectRepository extends JpaRepository<Subject, Integer>{

    Subject findByName(String name);

    Subject findById(Integer id);

}
