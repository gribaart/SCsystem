/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.repository;

import com.mycompany.spring.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author artgr
 */
public interface EventRepository extends JpaRepository<Event, Integer>{

//    @Query("select m from person_event m where m.person_id=:id")
//    List<Event> findByPersonId(@Param("id") Integer id);

    Event findByName(String name);

    Event  findById(Integer id);
}