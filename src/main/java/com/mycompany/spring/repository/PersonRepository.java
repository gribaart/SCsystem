/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.repository;

import com.mycompany.spring.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author artgr
 */
//@Repository
public interface PersonRepository extends JpaRepository<Person, Integer>{
    Person findById(Integer id);

//    @Query("select b from Person b where b.name = :name")
//    Person findByName(@Param("name") String name);
    Person findByName(String name);

//    @Query("select b from Person b where b.mail = :mail")
//    Person findByMail(@Param("mail") String name);
    Person findByMail(String name);


}
