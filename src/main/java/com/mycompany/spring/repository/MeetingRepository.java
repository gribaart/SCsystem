/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.repository;

import com.mycompany.spring.model.Meeting;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author artgr
 */
public interface MeetingRepository extends JpaRepository<Meeting, Integer>{

}
